function ends_in_3(num)
	x = tostring(num)
	y = string.len(x)
	if string.sub(x,y) == "3" then
		return true
	else
		return false
	end
end

function is_prime(num)
	divisible_by = {1}

  for i = 2, num do
    if num % i == 0 then
      table.insert(divisible_by, i)
    end
end
return #divisible_by < 3
end

function many_prime(num)
	i = 0
	found = 0
	while found < num do
	if 	ends_in_3(i) == true then
		if is_prime(i) == true then
		print(i)
		found = found + 1
		end
	end
	i = i + 1
end
end
many_prime(6)